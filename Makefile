all:
	CGO_ENABLED=0 GOOS=linux GOARCH=mips64 go build -ldflags '-buildid=' -trimpath -o nlaswitch .

.PHONY: all
