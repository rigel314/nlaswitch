function cmd(arg) {
	console.log(arg)
	let myRequest = new Request("/" + arg.toLowerCase());
	document.getElementById("message").innerText = "working...this can take a while..."
	fetch(myRequest)
		.then(function(resp) {
			if (!resp.ok) {
				document.getElementById("message").innerText = "bad response"
				return
			}
			resp.json().then(function(obj) {
				document.getElementById("message").innerText = obj
			})
		})
	return true
}

function get() {
	let myRequest = new Request("/status");
	document.getElementById("status").innerText = "checking..."
	fetch(myRequest)
		.then(function(resp) {
			if (!resp.ok) {
				document.getElementById("status").innerText = "bad response"
				return
			}
			resp.json().then(function(obj) {
				if (obj.Status != "success") {
					document.getElementById("status").innerText = obj.Status
					return
				}
				if (obj.Enabled) {
					document.getElementById("status").innerText = "ON"
				} else {
					document.getElementById("status").innerText = "OFF"
				}
			})
		})
	setTimeout(get, 10000)
}

function loaded() {
	get()
}
