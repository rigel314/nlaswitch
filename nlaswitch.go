package main

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"strings"
)

//go:embed www/*
var webFiles embed.FS

func main() {
	http.Handle("/www/", http.FileServer(http.FS(webFiles)))
	http.Handle("/", http.RedirectHandler("/www/", http.StatusFound))
	http.HandleFunc("/on", enableNLA)
	http.HandleFunc("/off", disableNLA)
	http.HandleFunc("/status", getNLA)

	log.Println("listening...")
	err := http.ListenAndServe("0.0.0.0:8080", nil)
	log.Fatal(err)
}

func enableNLA(rw http.ResponseWriter, req *http.Request) {
	setNLA(true, rw, req)
}
func disableNLA(rw http.ResponseWriter, req *http.Request) {
	setNLA(false, rw, req)
}

func setNLA(setting bool, rw http.ResponseWriter, req *http.Request) {
	ret := "failed"
	defer func() {
		jenc := json.NewEncoder(rw)
		jenc.Encode(ret)
	}()

	disabled := "true"
	if setting {
		disabled = "false"
	}

	cmdstr := fmt.Sprintf("source /opt/vyatta/etc/functions/script-template\nconfigure\nset service dns forwarding blacklist disabled %s\ncommit; save; exit", disabled)

	cmd := exec.Command("bash", "-c", cmdstr)
	err := cmd.Run()
	if err != nil {
		log.Println(err)
		return
	}

	ret = "success"
}

func getNLA(rw http.ResponseWriter, req *http.Request) {
	type retval struct {
		Status  string
		Enabled bool
	}
	ret := retval{Status: "failed"}
	defer func() {
		jenc := json.NewEncoder(rw)
		jenc.Encode(ret)
	}()

	buf := &bytes.Buffer{}
	cmd := exec.Command("bash", "-c", "/opt/vyatta/bin/vyatta-op-cmd-wrapper show configuration commands | grep 'set service dns forwarding blacklist disabled '")
	cmd.Stderr = nil
	cmd.Stdout = buf
	err := cmd.Run()
	if err != nil {
		// if _, ok := err.(*exec.ExitError); !ok {
		log.Println(err)
		return
		// }
	}

	words := strings.Split(buf.String(), " ")
	switch strings.TrimSpace(words[len(words)-1]) {
	case "true":
		ret.Enabled = false
	case "false":
		ret.Enabled = true
	default:
		log.Println("failed due to invalid output: ", buf.String())
		return
	}

	ret.Status = "success"
}
